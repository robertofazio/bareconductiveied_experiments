# Serial comunication between BareConductive and Unity3D #

IED Exercices Visual Art 2015-2016
Prof. Roberto Fazio

Serial Port should be /dev/cu.usbmodem1411 or /dev/cu.usbmodem1421 
Baud Rate: 57600

This is a simple Unity3D 5.3 project that allows you to comunicate with the Bareconductive board.
It works also with the Arduino UNO and MPR121 chipset.

### Links ###

* http://www.bareconductive.com/

Use the Arduino IDE 1.6.7 and sketch named "Touch_MP3"
