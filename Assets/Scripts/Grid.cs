﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Grid : MonoBehaviour {

	public GameObject myPrefab;
	public GameObject[,] griglia;
	public int x_res, y_res;
	public float x_gap, y_gap;

	public float perlinNoiseAnimX = 0.01f;
	public float perlinNoiseAnimY = 0.01f;
	public float noiseScale = 0.2f;
	public float heightScale = 3f;
	public bool movement;


	public Slider sl;
	public Slider sl1;
	public Toggle tg;

	public Slider sl3;
	public Slider sl4;

	public Gradient col;

	public float a,b,c, _a, _b, _c;


	//[Range (0, 1.00f)] 
	public float mv = 0.05f;
	//[Range (0.05f, 5.00f)] 
	public float mh = 1.00f;

	void Start () 
	{
		griglia = new GameObject[x_res, y_res];

		for (int i = 0; i < griglia.GetLength(0); i++) 
		{
			for (int j = 0; j < griglia.GetLength(1); j++) 
			{
				Vector3 startPosition = new Vector3(i * x_gap, j * y_gap, 0);
				griglia[i,j] = Instantiate(myPrefab, startPosition, Quaternion.identity ) as GameObject;
			}
		}

		Physics.gravity = new Vector3(100,0,0);
	}

	void Update () 
	{



		for(int i = 0; i < griglia.GetLength(0); i++)
		{
			for(int j = 0; j < griglia.GetLength(1); j++)
			{	
				float zPos = Mathf.PerlinNoise(i * noiseScale + perlinNoiseAnimX, j * noiseScale + perlinNoiseAnimY) * heightScale;
				Vector3 PerlinMovment = new Vector3(i * x_gap, j * y_gap, zPos);
				Vector3 SinMovment = new Vector3(i * x_gap, j * y_gap, Mathf.Sin(i * Time.time * mv)/mh);
				Vector3 position;

				if(movement)
					position = SinMovment;
				else
					position = PerlinMovment;
				
				griglia[i,j].transform.position = position;
				griglia[i,j].GetComponent<Renderer>().material.color = col.Evaluate(Mathf.Sin(i * Time.time * mv));

			
				RaycastHit hit;
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				Debug.DrawRay(ray.origin, ray.direction, Color.red);

				if (Input.GetMouseButtonDown(0)) 
				{
					


					if (Physics.Raycast(ray, out hit))
						
					if (hit.collider != null)
					{
						hit.collider.enabled = false;
						Debug.Log(hit.collider.name);
						griglia[i,j].AddComponent<Rigidbody>();


					}


				}
			}
		}

		perlinNoiseAnimX += 0.01f;
		perlinNoiseAnimY += 0.01f;

		noiseScale = sl.value;
		heightScale = sl1.value;
		movement = tg.isOn;
		mv = sl3.value;
		mh = sl4.value;


		this.transform.LookAt(griglia[10,10].transform);
		this.transform.position  = new Vector3(Mathf.Sin(a * Time.time) * _a, Mathf.Cos(b * Time.time) * _b, Mathf.Cos(c * Time.time) * _c);




	}


}
