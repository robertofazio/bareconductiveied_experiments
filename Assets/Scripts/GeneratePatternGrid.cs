﻿using UnityEngine;
using System.Collections;

public class GeneratePatternGrid : MonoBehaviour 
{
	public GameObject myPrefab;
	public GameObject[] grid;
	public int resolutionGrid = 10;
	public int gap = 2;
	public float mv = 1;

	void Start ()
	{
		grid = new GameObject[resolutionGrid];
		for(int x = 0; x < grid.Length; x++)
		{
			for(int y = 0; y < grid.Length; y++)
			{
				Vector3 StartPos = new Vector3(x * gap, y * gap, 0);
				grid[x] = Instantiate(myPrefab, myPrefab.transform.position + StartPos, Quaternion.Euler(new Vector3(0,0,0))) as GameObject;
			}
		}
	}
	
	void Update () 
	{
		for(int x = 0; x < grid.Length; x++)
		{
			for(int y = 0; y < grid.Length; y++)
			{
				grid[x].transform.localPosition = new Vector3(x * gap, y * gap, 0);
				//grid[y].transform.position = new Vector3(0, y * gap, 0);
			}
		}
			
	}
}
