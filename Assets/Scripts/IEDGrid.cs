﻿using UnityEngine;
using System.Collections;

public class IEDGrid : MonoBehaviour 
{

	// qui dichiaro le variabili
	// dichiaro una variabile pubblica Gameobject che mi ritrovo in Unity Editor 
	public GameObject myPrefab;
	// dichiaro variabile Vector3 con il nome posPrefab ( posizione del cubo x,y,z )
	//public Vector3 posPrefab;
	// dichiaro un array di Gameobject che si chiama arrayCubi
	public GameObject[,] arrayCubi;

	public int numMax_x;
	public int numMax_y;

	// Use this for initialization
	void Start () 
	{
		// assegnato all'arrayCubi il valore di 10
		arrayCubi = new GameObject[numMax_x, numMax_y];

		// i++ identico a i = i + 1
		for(int i = 0; i < arrayCubi.GetLength(0); i++)
		{
			for(int j = 0; j < arrayCubi.GetLength(1); j++)
			{
				arrayCubi[i,j] = Instantiate(myPrefab, new Vector3(i * 2, j * 2, 0) , Quaternion.identity) as GameObject;
			}
		}
	}


	
	// Update is called once per frame
	void Update () 
	{
		
	}


}
