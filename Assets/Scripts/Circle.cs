﻿using UnityEngine;
using System.Collections;

public class Circle : MonoBehaviour 
{

	public int numberOfObjects = 4;
	public float raggio = 200;

	public GameObject preFab;

	public GameObject[] alignToCircleObjects;
	public Vector3 pos;

	public float pv = 1;

	void Start () 
	{
		alignToCircleObjects = new GameObject[numberOfObjects];

		for(int i = 0; i < alignToCircleObjects.Length; i++)
		{
			float angolo = i * Mathf.PI * 2 /numberOfObjects;
			pos = new Vector3(Mathf.Cos(angolo), 0, Mathf.Sin(angolo)) * raggio;

			alignToCircleObjects[i] = Instantiate(preFab, pos, Quaternion.identity) as GameObject;
		}

			
	}
	

	void Update () 
	{
		for(int i = 0; i < alignToCircleObjects.Length; i++)
		{
			float angolo = i * Mathf.PI * 2 /numberOfObjects;


			alignToCircleObjects[i].transform.position = new Vector3(Mathf.Cos(angolo) * Mathf.PingPong(Time.time, pv) , 0, Mathf.Sin(angolo) * Mathf.PingPong(Time.time, pv)) * raggio;
		}
	}
}
