﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MovLighting : MonoBehaviour 
{
	public GameObject mainLight;
	private Light DirLight;
	public float intensity;
	public float mv;

	public Slider sl;


	void Start () 
	{
		DirLight = mainLight.GetComponent<Light>();
	}

	
	void Update () 
	{
		DirLight.intensity = Mathf.PingPong(Time.time * mv, 1);
		DirLight.color = new Color(sl.value, DirLight.color.g, DirLight.color.b, DirLight.color.a); 
	}
}
